package com.company.ags.lecturaqr

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.R.attr.data
import android.app.Activity
import android.content.Context
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import butterknife.BindView
import butterknife.ButterKnife
import com.google.zxing.integration.android.IntentIntegrator
import com.google.zxing.integration.android.IntentResult
import kotlinx.android.synthetic.main.activity_main.view.*
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.OutputStreamWriter
import java.util.*


class MainActivity : AppCompatActivity() {

    @BindView(R.id.btn_scanear)
    lateinit var btnEscanear: Button


    @BindView(R.id.text_result)
    lateinit var resultadoText: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)

        val activity: Activity = this

        btnEscanear.setOnClickListener(View.OnClickListener {
            val integrator = IntentIntegrator(activity)
            integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
            integrator.setPrompt("Scan")
            integrator.setCameraId(0)
            integrator.setBeepEnabled(false)
            integrator.setBarcodeImageEnabled(false)
            integrator.initiateScan()

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result: IntentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null) {
            if (result.contents == null) {
                Toast.makeText(this, "No encuentra codigo", Toast.LENGTH_SHORT).show()
            } else {
                resultadoText.text = null
                resultadoText.text = result.contents
                Toast.makeText(this, result.contents, Toast.LENGTH_SHORT).show()
                writeArcgivo(result.contents)

            }

        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }


    fun writeArcgivo(cadena: String) {
        try {
            val cadena = cadena + '\n'
            try {
                var baseFolder: String = ""
                if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                    baseFolder = this.getExternalFilesDir(null).getAbsolutePath()
                }else {
                    baseFolder = this.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS).absolutePath//this.getFilesDir().getAbsolutePath();
                }
                Log.e("************* ", baseFolder.toString())
                val file = File(baseFolder + '/' + "test.txt")
                val fos = FileOutputStream(file, true)
                fos.write(cadena.toByteArray())
                fos.close()
                Toast.makeText(this, "Guardado ruta "+ baseFolder.toString(),
                        Toast.LENGTH_SHORT).show()

            } catch (e: Exception) {
                e.printStackTrace()
            }
            /*val cacheDir = this.cacheDir
                Log.e("*******Amor******* ",cacheDir.absolutePath)
            val fileout = openFileOutput("mytextfile.txt", Context.MODE_APPEND)

            val outputWriter = OutputStreamWriter(fileout)
            outputWriter.write(cadena + '\n')
            outputWriter.close()

            //display file saved message*/
            Toast.makeText(this, "File saved successfully!",
                    Toast.LENGTH_SHORT).show()

        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}

